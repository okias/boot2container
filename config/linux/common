# Enable CGROUPS, for podman
CONFIG_CGROUPS=y
CONFIG_BLK_CGROUP=y
CONFIG_CGROUP_WRITEBACK=y
CONFIG_CGROUP_SCHED=y
CONFIG_CGROUP_PIDS=y
CONFIG_CGROUP_FREEZER=y
<% if [[ "${arch}" != "arm" ]]; then -%>
CONFIG_HUGETLB_PAGE=y
CONFIG_CGROUP_HUGETLB=y
<% fi -%>
CONFIG_CGROUP_DEVICE=y
CONFIG_CGROUP_CPUACCT=y
CONFIG_CGROUP_PERF=y
CONFIG_CGROUP_DEBUG=y
CONFIG_CGROUP_RDMA=y
CONFIG_SOCK_CGROUP_DATA=y
CONFIG_MEMCG=y
CONFIG_NET=y
CONFIG_NET_SCHED=y
CONFIG_NET_CLS_CGROUP=y
CONFIG_CGROUP_NET_CLASSID=y
CONFIG_CGROUP_NET_PRIO=y

# Enable user namespace, for podman
CONFIG_NAMESPACES=y
CONFIG_USER_NS=y

# Enable OVERLAYFS, for podman
CONFIG_OVERLAY_FS=y

# For being able to run nested containers (podman in podman)
CONFIG_VETH=y
<% if [[ "${arch}" != "arm" ]]; then -%>
CONFIG_NF_CONNTRACK=y
CONFIG_NF_NAT=y
CONFIG_NF_NAT_MASQUERADE=y
CONFIG_NETFILTER_XTABLES=y
CONFIG_NETFILTER_XT_MATCH_ADDRTYPE=y
CONFIG_NETFILTER_XT_MATCH_CONNTRACK=y
CONFIG_NETFILTER_XT_NAT=y
CONFIG_NETFILTER_XT_TARGET_MASQUERADE=y
CONFIG_IP_NF_IPTABLES=y
CONFIG_IP_NF_FILTER=y
CONFIG_IP_NF_NAT=y
CONFIG_IP_NF_TARGET_MASQUERADE=y
CONFIG_IP6_NF_IPTABLES=y
CONFIG_IP6_NF_FILTER=y
<% if [[ "$features" == *"netfilter"* ]]; then -%>
CONFIG_NETFILTER_XT_MATCH_COMMENT=y
CONFIG_NETFILTER_XT_MATCH_MARK=y
CONFIG_NETFILTER_XT_MATCH_MULTIPORT=y
CONFIG_IP6_NF_NAT=y
CONFIG_IP6_NF_TARGET_MASQUERADE=y
<% fi -%>
<% fi -%>
CONFIG_FUSE_FS=y
CONFIG_OVERLAY_FS_INDEX=y
<% if [[ "${arch}" != "arm" ]]; then -%>
CONFIG_OVERLAY_FS_XINO_AUTO=y
<% fi -%>

# For the fscrypt-based volume encryption
CONFIG_FS_ENCRYPTION=y
CONFIG_FS_ENCRYPTION_ALGS=y
CONFIG_CRYPTO_CRYPTD=y
CONFIG_CRYPTO_SHA1=y
CONFIG_CRYPTO_SHA256=y
CONFIG_CRYPTO_SHA512=y
CONFIG_CRYPTO_CTS=y
CONFIG_CRYPTO_ECB=y
CONFIG_CRYPTO_XTS=y

<% if [[ "${arch}" == "amd64" ]]; then -%>
CONFIG_CRYPTO_SIMD=y
CONFIG_CRYPTO_SHA1_SSSE3=y
CONFIG_CRYPTO_SHA256_SSSE3=y
CONFIG_CRYPTO_SHA512_SSSE3=y
CONFIG_CRYPTO_AES_TI=y
CONFIG_CRYPTO_AES_NI_INTEL=y
<% fi -%>


# Compression
<% if [[ "${arch}" == "amd64" ]]; then -%>
CONFIG_KERNEL_XZ=y
<% fi -%>
CONFIG_FW_LOADER_PAGED_BUF=y
CONFIG_FW_LOADER_COMPRESS=y

# Branding
CONFIG_LOCALVERSION="-B2C"
CONFIG_DEFAULT_HOSTNAME="boot2container"

# IO performance
CONFIG_IOSCHED_BFQ=y
CONFIG_BFQ_GROUP_IOSCHED=y

# CPU sharing
CONFIG_CFS_BANDWIDTH=y
CONFIG_RT_GROUP_SCHED=y

# Network bridge
CONFIG_BRIDGE=y
CONFIG_BRIDGE_IGMP_SNOOPING=y
CONFIG_BRIDGE_VLAN_FILTERING=y
CONFIG_VLAN_8021Q=y

# NVME support
CONFIG_NVME_CORE=y
CONFIG_BLK_DEV_NVME=y
CONFIG_NVME_FABRICS=y
CONFIG_NVME_FC=y
CONFIG_NVME_TCP=y

# External USB storage
CONFIG_USB_STORAGE_REALTEK=y
CONFIG_REALTEK_AUTOPM=y
CONFIG_USB_STORAGE_DATAFAB=y
CONFIG_USB_STORAGE_FREECOM=y
CONFIG_USB_STORAGE_ISD200=y
CONFIG_USB_STORAGE_USBAT=y
CONFIG_USB_STORAGE_SDDR09=y
CONFIG_USB_STORAGE_SDDR55=y
CONFIG_USB_STORAGE_JUMPSHOT=y
CONFIG_USB_STORAGE_ALAUDA=y
CONFIG_USB_STORAGE_ONETOUCH=y
CONFIG_USB_STORAGE_KARMA=y
CONFIG_USB_STORAGE_CYPRESS_ATACB=y
CONFIG_USB_STORAGE_ENE_UB6250=y
CONFIG_USB_UAS=y

# Disable unsupported networks
CONFIG_WIRELESS=n
CONFIG_WLAN=n
CONFIG_CFG80211=n
CONFIG_RFKILL=n
CONFIG_GNSS=n
CONFIG_CAN=n
CONFIG_BT=n

# Disable the Graphics subsystem
CONFIG_DRM=n

# Disable the media subsystem
CONFIG_MEDIA_SUPPORT=n

# Disable the sound subsystem
CONFIG_SOUND=n

# Disable the security subsystem as b2c does not benefit from it
CONFIG_SECURITY=n

# Disable XEN virtualization
CONFIG_XEN=n

# Disable unwanted filesystems
CONFIG_BTRFS_FS=n

# Disable FPGA support
CONFIG_FPGA=n

# Enable framebuffer support (this is much lighter than DRM)
CONFIG_VIDEO_CMDLINE=y
CONFIG_FB_NOTIFY=y
CONFIG_FB=y
CONFIG_FB_CFB_FILLRECT=y
CONFIG_FB_CFB_COPYAREA=y
CONFIG_FB_CFB_IMAGEBLIT=y
CONFIG_FB_FOREIGN_ENDIAN=y
CONFIG_FB_MODE_HELPERS=y
CONFIG_FB_TILEBLITTING=y
CONFIG_FB_EFI=y
CONFIG_FRAMEBUFFER_CONSOLE=y

# EFI
CONFIG_EFIVAR_FS=y
CONFIG_EFI_STUB=y
CONFIG_EFI=y
CONFIG_EFI_ESRT=y
CONFIG_EFI_RUNTIME_WRAPPERS=y
<% if [[ "${arch}" != "arm" ]]; then -%>
CONFIG_EFI_EARLYCON=y
<% fi -%>
<% if [[ "${arch}" != "arm" ]] && [[ "${arch}" != "riscv64" ]]; then -%>
CONFIG_DMI=y
CONFIG_DMIID=y
<% fi -%>
<% if [[ "${arch}" == "arm64" ]] || [[ "${arch}" == "riscv64" ]]; then -%>
CONFIG_EFI_ZBOOT=y
CONFIG_EFI_PARAMS_FROM_FDT=y
CONFIG_EFI_GENERIC_STUB=y
<% fi -%>
<% if [[ "${arch}" == "arm64" ]]; then -%>
CONFIG_EFI_ARMSTUB_DTB_LOADER=y
<% fi -%>

# x86_64
<% if [[ "${arch}" == "amd64" ]]; then -%>
CONFIG_FB_VESA=y
CONFIG_X86_AMD_PSTATE=y
CONFIG_X86_PKG_TEMP_THERMAL=y
CONFIG_EEEPC_LAPTOP=n
<% fi -%>

# Disable unsupported AARCH64 platforms
<% if [[ "${arch}" == "arm64" ]]; then -%>
CONFIG_ARCH_ACTIONS=n
CONFIG_ARCH_ALPINE=n
CONFIG_ARCH_BERLIN=n
CONFIG_ARCH_K3=n
CONFIG_ARCH_LAYERSCAPE=n
CONFIG_ARCH_LG1K=n
CONFIG_ARCH_HISI=n
CONFIG_ARCH_KEEMBAY=n
CONFIG_ARCH_MESON=n
CONFIG_ARCH_MVEBU=n
CONFIG_ARCH_MXC=n
CONFIG_ARCH_S32=n
CONFIG_ARCH_SEATTLE=n
CONFIG_ARCH_SYNQUACER=n
CONFIG_ARCH_TESLA_FSD=n
CONFIG_ARCH_SPRD=n
CONFIG_ARCH_THUNDER=n
CONFIG_ARCH_THUNDER2=n
CONFIG_ARCH_UNIPHIER=n
CONFIG_ARCH_VEXPRESS=n
CONFIG_ARCH_VISCONTI=n
CONFIG_ARCH_XGENE=n

CONFIG_NET_VENDOR_MELLANOX=n
<% fi -%>

# Set the performance governor by default
<% if [[ "${arch}" != "arm" ]] && [[ "${arch}" != "riscv64" ]]; then -%>
CONFIG_CPU_FREQ_STAT=y
CONFIG_CPU_FREQ_DEFAULT_GOV_PERFORMANCE=y
<% fi -%>

# Allow warnings when compiling
CONFIG_WERROR=n

# Disable the usb_onboard_hub driver to work around a regression
# Link: https://lore.kernel.org/all/d04bcc45-3471-4417-b30b-5cf9880d785d@i2se.com/
<% if [[ "${arch}" == "arm64" ]]; then -%>
CONFIG_USB_ONBOARD_HUB=n
<% fi -%>

# Disable the crypto-testing module
CONFIG_CRYPTO_TEST=n
