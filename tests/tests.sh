#!/bin/busybox sh

suite()
{
  UNITTEST="${UNITTEST:-1}"
  INTEGRATION="${INTEGRATION:-1}"
  VM2C="${VM2C:-1}"

  # Set up the folder that will hold the coverage data, and empty any existing coverage report
  rm -rf ./coverage/
  mkdir -p ./coverage/

  # Unittests
  if [ "$UNITTEST" -eq 1 ]; then
    . ./tests/unittests/container.sh
    . ./tests/unittests/cmdline.sh
    . ./tests/unittests/hooks.sh
    . ./tests/unittests/misc.sh
    . ./tests/unittests/network.sh
    . ./tests/unittests/volumes.sh
    . ./tests/unittests/go.sh
  fi

  # Integration
  if [ "$INTEGRATION" -eq 1 ]; then
    . ./tests/integration/misc.sh
    . ./tests/integration/containers.sh
    . ./tests/integration/volumes.sh
  fi

  # vm2c
  if [ "$VM2C" -eq 1 ]; then
    . ./tests/vm2c/vm2c.sh
  fi
}

oneTimeTearDown()
{
  echo 'mode: set' > ./coverage/combined.cov
  tail -q -n +2 ./coverage/*.out >> ./coverage/combined.cov 2> /dev/null

  go tool cover -html=./coverage/combined.cov -o ./coverage/coverage.html
}

# HACK: Make sure the initscript knows where to find the run_cmd_in_loop command
cp ./run_cmd_in_loop.sh /bin/run_cmd_in_loop.sh

# Load shUnit2.
stdout=""
cmdline=""
exit_code=""
. /usr/bin/shunit2
