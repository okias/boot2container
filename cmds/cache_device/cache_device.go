package main

import (
	"log"
	"os"

	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/cache_device"
	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/cmdline"
	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/filesystem"
)

const (
	STORAGE_MOUNTPOINT        = "/storage"
	STORAGE_MOUNTPOINT_LEGACY = "/container"

	CACHE_DEVICE_OPT_NAME = "b2c.cache_device"
	FILESYSTEM_OPT_NAME   = "b2c.filesystem"
)

func main() {
	// Start by creating the mountpoints, and linking
	if err := os.MkdirAll(STORAGE_MOUNTPOINT, 0755); err != nil {
		log.Panicf("Failed to create the '%s' mountpoint: %s\n", STORAGE_MOUNTPOINT, err.Error())
	}
	if err := os.Symlink(STORAGE_MOUNTPOINT, STORAGE_MOUNTPOINT_LEGACY); err != nil {
		log.Panicf("Failed to create the '%s' legacy mountpoint: %s\n", STORAGE_MOUNTPOINT_LEGACY, err.Error())
	}

	// Parse the wanted configuration
	cd_opt, _ := cmdline.FindOption(cmdline.OptionQuery{Name: CACHE_DEVICE_OPT_NAME})
	fs_opt, _ := cmdline.FindOption(cmdline.OptionQuery{Name: FILESYSTEM_OPT_NAME})
	fses := filesystem.ListCmdlineFilesystems(fs_opt)
	cfg := cache_device.ParseCmdline(cd_opt, fses)

	cfg.Mount(STORAGE_MOUNTPOINT)
}
